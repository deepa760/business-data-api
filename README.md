# Business Data API
This project is used as a basis for DevOps projects.

## Information on the database connection for the business-data-api.  
The information for the database connection is read by the application from the src/main/resources/application.properties file. For security reasons the actual address username and password is not included in the repository but rather variables enclosed by @ signs (see file).  
You can choose yourself how to handle this replacement but on option is to use maven filtering (study the topic yourself).
Inspecting the pom.xml you can see that a resources section is included which supports the maven goal resources.
  
Example of command line usage: mvn package resources:resources -Dport="actual port" -Ddburl="actual database url" -Ddbuser="actual username" -Ddbpassword="actual password"
