FROM openjdk:8-jre-slim

COPY target/*.jar /home/app/business-api.jar

ENTRYPOINT [ "java", "-jar", "/home/app/business-api.war", "com.company.businessdataapi.Application" ]